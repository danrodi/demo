import Home from '../../../src/components/pages/Home'
import VueRouter from 'vue-router';
import { createLocalVue } from '@vue/test-utils';
import { mount } from '@vue/test-utils'

const localVue = createLocalVue();
localVue.use(VueRouter);

describe('Home', () => {
  let wrapper;

  beforeEach(() => {
    const props = {};
    wrapper = mount(Home, { props });
  });

  it('Should render correct contents', () => {
    expect(wrapper.find('h1').text()).toBe('Welcome');
  })
})
