export default {
  methods: {
    formatDate (date) {
      const d = new Date(0) // The 0 there is the key, which sets the date to the epoch
      d.setUTCSeconds(date)

      return d
    }
  }
}
