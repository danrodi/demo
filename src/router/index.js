import Vue from 'vue'
import Router from 'vue-router'

import Home from '@/components/pages/Home'
import News from '@/components/pages/News'
import Contact from '@/components/pages/Contact'
import AboutMe from '@/components/pages/AboutMe'
import AboutMyDog from '@/components/pages/AboutMyDog'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/home',
      name: 'Home',
      component: Home,
      alias: '/'
    },
    {
      path: '/news',
      name: 'News',
      component: News
    },
    {
      path: '/contact',
      name: 'Contact',
      component: Contact
    },
    {
      path: '/about-me',
      name: 'AboutMe',
      component: AboutMe
    },
    {
      path: '/about-my-dog',
      name: 'AboutMyDog',
      component: AboutMyDog
    }
  ]
})
